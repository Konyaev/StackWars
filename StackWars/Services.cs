﻿using System;
using System.IO;
namespace StackWars
{
    public static class Services
    {
        public static Random random = new Random();

		public static int WeightedRandom(int[] weights)
		{
            int arrayLength = weights.Length;
			int[] totals = new int[arrayLength];
			int runningTotal = 0;

			for (int i = 0; i < arrayLength; i++)
			{
				runningTotal += weights[i];
				totals[i] = runningTotal;
			}

			int rand = random.Next(runningTotal);
            int result = 0;
            while (rand >= totals[result])
            {
                result++;
            }

            return result;
		}

        public static bool ProbabilityBasedRandom(double trueProbability)
        {
            bool result = false;
            if (trueProbability >= 0 && trueProbability < 1)
                if (random.NextDouble() < trueProbability)
                    result = true;

            return result;
        }

        public static void WrightInFile(string logString, string filePath)
        {
            FileStream fileStream = new FileStream(filePath, FileMode.Append);
			StreamWriter file = new StreamWriter(fileStream);
			file.WriteLine(logString);
			file.Close();
        }
    }
}
