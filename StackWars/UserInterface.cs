﻿using System;
using System.Collections.Generic;

namespace StackWars
{
    public class UserInterface
    {
        private Stack<ICommand> commands = new Stack<ICommand>();

        private readonly Engine engine = Engine.Instance;

        private ICommand lastCancelledCommand;

        public void ShowMenuCommands()
        {
			Console.WriteLine("-----------------------------------------");
            Console.WriteLine("1 - Новый ход");
            Console.WriteLine("2 - Отменить последний ход");
            Console.WriteLine("3 - Вернуться на последний отменённый ход");
            Console.WriteLine("4 - Игра до конца");
            Console.WriteLine("5 - Изменить стратегию:");
            Console.WriteLine("    1 - Один на один (по умолчанию)");
            Console.WriteLine("    2 - Три на три");
            Console.WriteLine("    3 - Стенка на стенку");
            Console.WriteLine("6 - Отобразить армии");
			Console.WriteLine("0 - Выход");
			Console.WriteLine("-----------------------------------------");
        }

        public void Start()
        {
            ShowMenuCommands();
            ReadConsoleCommand();
        }

        private void NextTurn()
        {
			ICommand command = new Command();
			command.Execute();
			commands.Push(command);
		}

        private void ReadConsoleCommand()
        {
            Console.Write("Введите команду: ");
			char commandKey = Console.ReadKey().KeyChar;
            Console.WriteLine();
			switch (commandKey)
			{
				case '1': 
                    try
                    {
                        NextTurn();
                    }
                    catch (Exception e)
                    {
                        Console.WriteLine(e.Message);
                    }
                    break;
				case '2':
                    if (commands.Count > 0)
                    {
                        lastCancelledCommand = commands.Pop();
                        lastCancelledCommand.Undo();
                    }
                    else
                        Console.WriteLine("Ещё не сделано ни одного хода");
					break;
				case '3':
                    if (lastCancelledCommand != null)
                        lastCancelledCommand.Redo();
                    else
                        Console.WriteLine("Нет отменённых ходов");
                    break;
				case '4':
					while (engine.GameOver == false)
					{
                        NextTurn();
					}
                    Console.WriteLine("Игра окончена");
					break;
				case '5':
                    SubCommandLink5:
                    Console.Write("Введите субкоманду: ");
                    commandKey = Console.ReadKey().KeyChar;
                    Console.WriteLine();
                    IStrategy strategy;
                    switch(commandKey)
                    {
						case '1':
                            strategy = new StrategyOneAgainstOne();
                            Console.WriteLine("Стратегия изменена на 'Один на один'");
							break;
						case '2':
                            strategy = new StrategyThreeAgainstThree();
                            Console.WriteLine("Стратегия изменена на 'Три на три'");
							break;
                        case '3':
                            strategy = new StrategyAllAgainstAll();
                            Console.WriteLine("Стратегия изменена на 'Стенка на стенку'");
                            break;
                        default:
                            Console.Write("Несуществующая субкоманда. Попробуйте ещё раз: ");
                            goto SubCommandLink5;
                    }
                    try
                    {
                        engine.Strategy = strategy;
                    }
                    catch (Exception e)
                    {
                        Console.WriteLine(e.Message);
                    }
					break;
				case '6':
                    Console.WriteLine("Первая армия:");
                    foreach (IUnit unit in engine.ArmyOne)
                    {
                        Console.WriteLine(unit.ToString());
                    }
                    Console.WriteLine();
                    Console.WriteLine("Вторая армия:");
                    foreach (IUnit unit in engine.ArmyTwo)
					{
                        Console.WriteLine(unit.ToString());
					}
					break;
				case '0':
                    Console.WriteLine("Конец игры");
                    Environment.Exit(0);
					break;
				default:
					Console.Write("Несуществующая команда. Попробуйте ещё раз: ");
                    ReadConsoleCommand();
                    break;
			}
            ReadConsoleCommand();
        }
    }
}
