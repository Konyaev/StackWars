﻿using System;
namespace StackWars
{
    public interface IObserver
    {
        void Update(IUnit deadUnit);
    }
}
