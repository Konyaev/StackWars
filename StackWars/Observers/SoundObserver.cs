﻿using System;
namespace StackWars
{
    public class SoundObserver : IObserver
    {
        public void Update(IUnit deadUnit)
        {
            Console.Beep();
        }
    }
}
