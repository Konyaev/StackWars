﻿using System;
namespace StackWars
{
    public class LogObserver : IObserver
    {
        private string logFilePath = "../../Observers/DeathsLog.log";

		public void Update(IUnit deadUnit)
		{
            Services.WrightInFile(deadUnit.ToString(), logFilePath);
		}
    }
}
