﻿using System;
namespace StackWars
{
    public interface ICommand
    {
        void Execute();

        void Undo();

        void Redo();
    }
}
