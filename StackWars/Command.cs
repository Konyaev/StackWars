﻿using System;
using System.Collections.Generic;
namespace StackWars
{
    public class Command : ICommand
    {
        private List<IUnit> armyOneBefore;
        private List<IUnit> armyTwoBefore;
        private List<IUnit> armyOneAfter;
        private List<IUnit> armyTwoAfter;

        private Engine engine;

        public Command()
        {
            engine = Engine.Instance;

            CloneArmy(ref armyOneBefore, engine.ArmyOne);
            CloneArmy(ref armyTwoBefore, engine.ArmyTwo);
        }

		public void Execute()
        {
            engine.Round();

            CloneArmy(ref armyOneAfter, engine.ArmyOne);
            CloneArmy(ref armyTwoAfter, engine.ArmyTwo);
        }

		public void Undo()
        {
            engine.ArmyOne = armyOneBefore;
			engine.ArmyTwo = armyTwoBefore;
        }

		public void Redo()
        {
            engine.ArmyOne = armyOneAfter;
            engine.ArmyTwo = armyTwoAfter;
        }

        private void CloneArmy(ref List<IUnit> armyTo, List<IUnit> armyFrom)
		{
            armyTo = new List<IUnit>();
            foreach (IUnit unit in armyFrom)
			{
                armyTo.Add(unit.Clone());
			}
		}
    }
}
