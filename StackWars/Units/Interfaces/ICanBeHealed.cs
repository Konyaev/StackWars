﻿using System;
namespace StackWars
{
    public interface ICanBeHealed
    {
        int MaxHealth
        {
            get;
        }
    }
}
