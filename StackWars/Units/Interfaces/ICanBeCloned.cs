﻿using System;
namespace StackWars
{
    public interface ICanBeCloned
    {
        IUnit Clone();
    }
}
