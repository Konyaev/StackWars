﻿using System;
using System.Collections.Generic;
namespace StackWars
{
    public interface ISpecialAbility
    {
        int Range
        {
            get;
        }

		int Strength
		{
			get;
		}

        double ActionProbability
        {
            get;
        }

        void DoSpecialAction(IUnit receiver, List<IUnit> baseArmy);

        bool CanDoSpecialActionWith(IUnit unit);
    }
}
