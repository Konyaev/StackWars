﻿using System;
namespace StackWars
{
    public interface IUnit
    {
		int Health
		{
			get;
		}

        int Defence
        {
            get;
        }

        int Damage
        {
            get;
        }

        void ChangeHealth(int newHealth);

        string ToString();

        IUnit Clone();

		void AddObserver(IObserver o);

		void RemoveObserver(IObserver o);
    }
}
