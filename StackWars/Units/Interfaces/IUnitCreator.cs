﻿using System;
namespace StackWars
{
    public interface IUnitCreator
    {
        ushort Cost
        {
            get;
        }

        IUnit CreateUnit();
    }
}