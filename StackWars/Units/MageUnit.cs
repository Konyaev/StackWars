﻿using System;
using System.Collections.Generic;
namespace StackWars
{
	public class MageCreator : IUnitCreator
	{
		public ushort Cost
		{
			get;
		}

		public MageCreator()
		{
			Cost = 50;
		}

		public IUnit CreateUnit()
		{
			return new MageUnit();
		}
	}

    public class MageUnit : Unit, ICanBeHealed, ISpecialAbility, ICanBeCloned
	{
		public int Range
		{
			get;
		}

		public int Strength
		{
			get;
		}

		public int MaxHealth
		{
			get;
		}

		public double ActionProbability
		{
			get;
		}

        public MageUnit()
        {
			Health = 25;
			MaxHealth = Health;
			Damage = 3;
			Defence = 1;
			Strength = 1;
			Range = 1;
            ActionProbability = 0.01;
        }

        public void DoSpecialAction(IUnit receiver, List<IUnit> baseArmy)
        {
            baseArmy.Add(((ICanBeCloned)receiver).Clone());
        }

        public bool CanDoSpecialActionWith(IUnit unit)
        {
            if (unit is ICanBeCloned)
                return true;
            return false;
        }

		public override IUnit Clone()
		{
            IUnit newUnit = new MageUnit();
			newUnit.ChangeHealth(Health);
			return newUnit;
		}
	}
}
