﻿using System;
using System.Collections.Generic;
namespace StackWars
{
    public class Shield : Decorator
    {
		public override int Defence
		{
			get
			{
				return unit.Defence + 2;
			}
		}

		public Shield(IUnit component, List<IUnit> baseArmy) : base(component, baseArmy)
		{
		}
    }
}
