﻿using System;
using System.Collections.Generic;
namespace StackWars
{
    public class Horse : Decorator
    {
		public override int Defence
		{
			get
			{
				return unit.Defence + 3;
			}
		}

		public override int Damage
		{
			get
			{
				return unit.Damage + 3;
			}
		}

		public Horse(IUnit component, List<IUnit> baseArmy) : base(component, baseArmy)
        {
		}
    }
}
