﻿using System;
using System.Collections.Generic;
namespace StackWars
{
    public abstract class Decorator : IUnit, IArmored
    {
        private readonly double DecoratorDeletionProbability = 0.5;

        private List<IUnit> baseArmy;

        protected IUnit unit;

        protected Decorator(IUnit unit, List<IUnit> baseArmy)
        {
            if (!(unit is IArmored))
            {
                throw new Exception("Декорирование данного типа не поддерживается");
            }
            bool unitAlreadyHasThisTypeOfDecorator = GetUnitDecorators(unit).Contains(this.GetType());
            if (!unitAlreadyHasThisTypeOfDecorator)
            {
                this.unit = unit;
                this.baseArmy = baseArmy;
            }
            else
                throw new Exception("Unit has already been decorated by this decorator type");
        }

        public static List<Type> GetUnitDecorators(IUnit unit)
        {
            List<Type> decorators = new List<Type>();
            while(unit is Decorator)
            {
                decorators.Add(unit.GetType());
                unit = ((Decorator)unit).GetComponent();
            }
            return decorators;
        }

        public IUnit GetComponent()
        {
            return unit;
        }

		public virtual int Health
		{
            get
            {
                return unit.Health;
            }
        }

		public virtual int Defence
		{
			get
			{
                return unit.Defence;
			}
		}

		public virtual int Damage
        {
            get
            {
                return unit.Damage;
            }
		}

        public void ChangeHealth(int newHealth)
        {
            if (Services.ProbabilityBasedRandom(DecoratorDeletionProbability))
            {
                int index = baseArmy.IndexOf(unit);
                if (index >= 0) //если это "верхний" декоратор
                    baseArmy[index] = unit;
            }
            unit.ChangeHealth(newHealth);
        }

		public sealed override string ToString()
		{
            return unit.ToString() + string.Format("[{0}]",this.GetType().Name);
		}

        public IUnit Clone()
        {
            IUnit newUnit = (IUnit)Activator.CreateInstance(this.GetType(), unit.Clone(), baseArmy);
            newUnit.ChangeHealth(Health);
            return newUnit;
        }

		public void AddObserver(IObserver o)
        {
            unit.AddObserver(o);
        }

		public void RemoveObserver(IObserver o)
        {
            unit.RemoveObserver(o);
        }
    }
}
