﻿using System;
using System.Collections.Generic;
namespace StackWars
{
    public class LongSword : Decorator
    {
		public override int Damage
		{
			get
			{
				return unit.Damage + 2;
			}
		}

        public LongSword(IUnit component, List<IUnit> baseArmy) : base(component, baseArmy)
        {
        }
    }
}
