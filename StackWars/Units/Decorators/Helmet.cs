﻿using System;
using System.Collections.Generic;
namespace StackWars
{
    public class Helmet : Decorator
    {
		public override int Defence
		{
			get
			{
				return unit.Defence + 1;
			}
		}

		public Helmet(IUnit component, List<IUnit> baseArmy) : base(component, baseArmy)
		{
		}
    }
}
