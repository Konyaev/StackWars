﻿using System;
using System.Collections.Generic;
namespace StackWars
{
	public class ClericCreator : IUnitCreator
	{
		public ushort Cost
		{
			get;
		}

		public ClericCreator()
		{
			Cost = 40;
		}

		public IUnit CreateUnit()
		{
			return new ClericUnit();
		}
	}

    public class ClericUnit : Unit, ICanBeHealed, ISpecialAbility, ICanBeCloned
	{
		public int Range
		{
			get;
		}

		public int Strength
		{
			get;
		}

		public int MaxHealth
		{
			get;
		}

		public double ActionProbability
		{
			get;
		}

        public ClericUnit()
        {
			Health = 20;
			MaxHealth = Health;
			Damage = 1;
			Defence = 0;
			Strength = -3;
			Range = 1;
            ActionProbability = 0.5;
        }

        public void DoSpecialAction(IUnit receiver, List<IUnit> baseArmy)
        {
            if (receiver.Health - Strength > ((ICanBeHealed)receiver).MaxHealth)
                receiver.ChangeHealth(((ICanBeHealed)receiver).MaxHealth);
            else
                receiver.ChangeHealth(receiver.Health - Strength);
		}

		public bool CanDoSpecialActionWith(IUnit unit)
		{
			if (unit is ICanBeHealed)
				return true;
			return false;
		}

        public override IUnit Clone()
        {
            IUnit newUnit = new ClericUnit();
            newUnit.ChangeHealth(Health);
            return newUnit;
        }
    }
}
