﻿using System;
using System.Collections.Generic;
using System.Reflection;
using System.Linq;
namespace StackWars
{
	public class InfantryCreator : IUnitCreator
	{
        public ushort Cost
        {
            get;
        }

        public InfantryCreator()
        {
            Cost = 20;
        }

		public IUnit CreateUnit()
		{
			return new InfantryUnit();
		}
	}

    public class InfantryUnit : Unit, ICanBeHealed, ISpecialAbility, ICanBeCloned
    {
		public int Range
		{
			get;
		}

		public int Strength
		{
			get;
		}

        public int MaxHealth
        {
            get;
        }

		public double ActionProbability
		{
			get;
		}

        public InfantryUnit() : base()
        {
            Health = 10;
            MaxHealth = Health;
            Damage = 2;
            Defence = 1;
			Strength = 0;
			Range = 1;
			ActionProbability = 0.2;
        }

		public void DoSpecialAction(IUnit receiver, List<IUnit> baseArmy) //декорирует receiver
		{
            int index = baseArmy.IndexOf(receiver);
            if (index >= 0)
            {
                List<Type> decorators = typeof(Decorator)
				    .GetTypeInfo().Assembly.GetTypes()
                    .Where(t => t.GetTypeInfo().IsSubclassOf(typeof(Decorator)))
                    .ToList();
                decorators.RemoveAll(type => Decorator.GetUnitDecorators(receiver).Contains(type));
                //к этому моменту в decorators только те типы декораторов, которых ещё нет у юнита

                int decoratorsLength = decorators.Count();
                if (decoratorsLength > 0) //если ещё не все декораторы применены (не может быть 2 шлема, например, поэтому всего по 1)
                {
                    int decoratorIndex = Services.random.Next(decoratorsLength);
					Type decoratorType = decorators.ElementAt(decoratorIndex);
					IUnit decorator = (IUnit)Activator.CreateInstance(decoratorType, receiver, baseArmy);
					baseArmy[index] = decorator;                                   
                }
            }
            else
                throw new Exception("Receiver should belong to baseArmy");
		}

		public bool CanDoSpecialActionWith(IUnit unit)
		{
            if (unit is IArmored)
				return true;
			return false;
		}

        public override IUnit Clone()
        {
            IUnit newUnit = new InfantryUnit();
            newUnit.ChangeHealth(Health);
            return newUnit;
        }
    }
}
