﻿using System;
using System.Collections.Generic;
namespace StackWars
{
	public class ArcherCreator : IUnitCreator
	{
		public ushort Cost
		{
			get;
		}

		public ArcherCreator()
		{
			Cost = 40;
		}

		public IUnit CreateUnit()
		{
			return new ArcherUnit();
		}
	}

    public class ArcherUnit: Unit, ICanBeHealed, ISpecialAbility, ICanBeCloned
    {
		public int Range
		{
			get;
		}

		public int Strength
		{
			get;
		}

		public int MaxHealth
		{
			get;
		}

		public double ActionProbability
		{
			get;
		}

        public ArcherUnit()
        {
			Health = 15;
			MaxHealth = Health;
			Damage = 4;
			Defence = 1;
            Strength = 2;
            Range = 5;
            ActionProbability = 0.8;
        }

		public void DoSpecialAction(IUnit receiver, List<IUnit> baseArmy)
		{
            receiver.ChangeHealth(receiver.Health - Strength);
		}

		public bool CanDoSpecialActionWith(IUnit unit)
		{
			return true;
		}

		public override IUnit Clone()
		{
            IUnit newUnit = new ArcherUnit();
			newUnit.ChangeHealth(Health);
			return newUnit;
		}
    }
}
