﻿using System;
namespace StackWars
{
	public class ArmoredCreator : IUnitCreator
	{
		public ushort Cost
		{
			get;
		}

		public ArmoredCreator()
		{
			Cost = 30;
		}

		public IUnit CreateUnit()
        {
            return new ArmoredUnitProxy();
		}
	}

    public class ArmoredUnitProxy : IUnit, IArmored
	{
        private ArmoredUnit realArmoredUnit;

        private string logFilePath = "../../Units/Armored.log";

        public int Health
        {
            get
            {
                Log("Get health");
                return realArmoredUnit.Health;
            }
            protected set
            {
                Log("Set health");
                realArmoredUnit.ChangeHealth(value);
            }
        }

        public int Defence
        {
            get
            {
                Log("Get defence");
                return realArmoredUnit.Defence;
            }
        }

        public int Damage
        {
            get
            {
                Log("Get damage");
                return realArmoredUnit.Damage;
            }
        }

        public ArmoredUnitProxy()
		{
            Log("New Armored Unit");
            realArmoredUnit = new ArmoredUnit();
		}

        public void ChangeHealth(int newHealth)
        {
            realArmoredUnit.ChangeHealth(newHealth);
        }

        public IUnit Clone()
        {
            Log("Clone");
            IUnit newUnit = new ArmoredUnitProxy();
            newUnit.ChangeHealth(Health);
            return newUnit;
        }

		public override string ToString()
		{
            return realArmoredUnit.ToString();
		}

        public void AddObserver(IObserver o)
		{
            Log("Add observer");
            realArmoredUnit.AddObserver(o);
		}

        public void RemoveObserver(IObserver o)
		{
            Log("Remove observer");
            realArmoredUnit.RemoveObserver(o);
		}

        private void Log(string logString)
        {
            Services.WrightInFile(string.Format("{0} {1}", this.GetHashCode(), logString), logFilePath);
        }
	}

    public class ArmoredUnit : Unit, IArmored
    {
        public ArmoredUnit() : base()
        { 
			Health = 10;
			Defence = 2;
			Damage = 5;
        }

		public override IUnit Clone()
		{
            IUnit newUnit = new ArmoredUnit();
			newUnit.ChangeHealth(Health);
			return newUnit;
		}

    }
}
