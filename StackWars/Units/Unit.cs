﻿using System;
using System.Collections.Generic;
namespace StackWars
{
    public abstract class Unit : IUnit
    {
        protected List<IObserver> observers = new List<IObserver>();

		public virtual int Health
		{
            get;
            protected set;
		}

		public virtual int Defence
		{
			get;
            protected set;
		}

		public virtual int Damage
		{
			get;
            protected set;
		}

        protected Unit()
        {
			observers.Add(new LogObserver());
			observers.Add(new SoundObserver());
        }

        public void ChangeHealth(int newHealth)
        {
            Health = newHealth;
            if (newHealth <= 0)
			{
				NotifyObservers();
			}
        }

        public abstract IUnit Clone();

        public sealed override string ToString()
        {
            return string.Format("[{0}: Health={1}]", this.GetType().Name, Health);
        }

		public virtual void AddObserver(IObserver o)
		{
			observers.Add(o);
		}

        public virtual void RemoveObserver(IObserver o)
		{
			observers.Remove(o);
		}

        protected void NotifyObservers()
		{
			foreach (IObserver observer in observers)
				observer.Update(this);
		}
    }
}
