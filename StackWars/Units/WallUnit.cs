﻿using System;
using SpecialUnits;
namespace StackWars
{
	public class WallCreator : IUnitCreator
	{
		public ushort Cost
		{
			get;
		}

        public WallCreator()
		{
			Cost = 60;
		}

		public IUnit CreateUnit()
		{
            return new WallUnit(Cost);
		}
	}

    public class WallUnit : Unit
    {
        private readonly GulyayGorod gulyayGorod;

        public override int Health
        {
            get => gulyayGorod.GetCurrentHealth();
            protected set
            {
                int damage = Health - value;
                try
                {
                    gulyayGorod.TakeDamage(damage);
                }
                catch
                {
                }
            }
        }

        public override int Defence
        {
            get
            {
                return gulyayGorod.GetDefence();
            }
        }

        public override int Damage
        {
            get
            {
                return gulyayGorod.GetStrength();
            }
        }

        public WallUnit(int cost) : base()
        {
            gulyayGorod = new GulyayGorod(health: 50, defence: 0, cost: cost);
        }

        public override IUnit Clone()
        {
            IUnit newUnit = new WallUnit(gulyayGorod.GetCost());
            newUnit.ChangeHealth(Health);
            return newUnit;
        }
    }
}
