﻿using System;
using System.Collections.Generic;
namespace StackWars
{
    public static class ArmyCreator
    {
        public static readonly int totalUnitsCost = 1000;

        private static readonly IUnitCreator[] creators = {
            new InfantryCreator(),
            new ArmoredCreator(),
            new ArcherCreator(),
            new ClericCreator(),
            new MageCreator(),
            new WallCreator()
        };

        public static List<IUnit> CreateArmy()
        {
            List<IUnit> army = new List<IUnit>();

            int remainingMoneyPoints = totalUnitsCost;
            while (remainingMoneyPoints > 0)
            {
                army.Add(CreateUnit(ref remainingMoneyPoints));
            }

            return army;
        }

        private static IUnit CreateUnit(ref int remainingMoneyPoints)
        {
			int arrayLength = creators.Length;
            ushort[] classesCosts = new ushort[arrayLength];
            classesCosts[0] = creators[0].Cost;
			ushort minCost = classesCosts[0];
			ushort maxCost = classesCosts[0];
			for (int i = 1; i < arrayLength; i++)
			{
				classesCosts[i] = creators[i].Cost;
				if (classesCosts[i] < minCost)
					minCost = classesCosts[i];
				if (classesCosts[i] > maxCost)
					maxCost = classesCosts[i];
			}

			//вероятность создания экземпляра класса обратнопропорциональна его Cost
			int[] classesAppearanceProbabilities = new int[arrayLength];
			for (int i = 0; i < arrayLength; i++)
			{
				classesAppearanceProbabilities[i] = maxCost - (classesCosts[i] - minCost);
			}

            int randomCreatorNumber = Services.WeightedRandom(classesAppearanceProbabilities);
            remainingMoneyPoints -= creators[randomCreatorNumber].Cost;

            return creators[randomCreatorNumber].CreateUnit();
        }
    }
}