﻿using System;
using System.Collections.Generic;
namespace StackWars
{
    public class StrategyThreeAgainstThree : IStrategy
    {
		public List<IUnit> GetSpecialActionReceiversByRange(ISpecialAbility unit, int position, List<IUnit> baseArmy, List<IUnit> enemyArmy)
		{
            List<IUnit>[] baseArmyColumns = new List<IUnit>[3];
            for (int i = 0; i < baseArmyColumns.Length; i++)
                baseArmyColumns[i] = new List<IUnit>();
            int unitColumn = 0; //значение по умолчанию, будет изменено в ближайшем for
            int unitPosition = -1;
			for (int i = 0; i < baseArmy.Count; i++)
			{
                int column = i % 3;
                baseArmyColumns[column].Add(baseArmy[i]);
                if (baseArmy[i] == unit)
                {
                    unitPosition = i;
					unitColumn = column;
				}
			}
            if (unitPosition == -1)
                throw new Exception("'unit' должен находиться в 'baseArmy'");

            List<IUnit> receiversList = new List<IUnit>();

			if (unit.Strength > 0) //enemy army action
			{
				List<IUnit>[] enemyArmyColumns = new List<IUnit>[3];
				for (int i = 0; i < enemyArmyColumns.Length; i++)
					enemyArmyColumns[i] = new List<IUnit>();
                for (int i = 0; i < enemyArmy.Count; i++)
                    enemyArmyColumns[i % 3].Add(enemyArmy[i]);
                
                //пройдём по вражеской армии от самых дальних возможных юнитов (определяется Range) до ближних
                int remainingRange = unit.Range - unitPosition;
                while (remainingRange > 0)
				{
                    for (int i = 0; i < 3; i++) //проходим по всем 3 колоннам вражеской армии
                    {
                        //позиция врага это текущая дальность атаки (remainingRange)
                        //  минус разница колонны врага с колонной атакующего юнита
                        //  минус 1 (так как позиция отсчитывается от 0, а дальность атаки от 1)
						int enemyUnitPosition = remainingRange - Math.Abs(i - unitColumn) - 1;
                        if (enemyUnitPosition >= 0 && enemyUnitPosition < enemyArmyColumns[i].Count) //если подсчитанная здесь позиция врага находится в границах вражеской армии
						    if (unit.CanDoSpecialActionWith(enemyArmyColumns[i][enemyUnitPosition]))
							    receiversList.Add(enemyArmyColumns[i][enemyUnitPosition]);
                    }
                    remainingRange--;
				}
			}
			else //base army action
			{
				int remainingRange = unit.Range;
				while (remainingRange > 0)
				{
					for (int i = 0; i < 3; i++) //проходим по всем 3 колоннам армии
					{
                        int targetUnitPosition = unitPosition + remainingRange - Math.Abs(i - unitColumn);
                        bool targetUnitPositionInsideArmy = targetUnitPosition >= 0 && targetUnitPosition < baseArmyColumns[i].Count;
                        if (targetUnitPositionInsideArmy) //если позиция целевого юнита при текущей дальности выходит за границы армии, ничего не делаем
	                        if (unit.CanDoSpecialActionWith(baseArmyColumns[i][targetUnitPosition]))
	                            receiversList.Add(baseArmyColumns[i][targetUnitPosition]);
                        
						targetUnitPosition = unitPosition + remainingRange - Math.Abs(i - unitColumn);
						if (targetUnitPositionInsideArmy) //если позиция целевого юнита при текущей дальности выходит за границы армии, ничего не делаем
							if (unit.CanDoSpecialActionWith(baseArmyColumns[i][targetUnitPosition]))
								receiversList.Add(baseArmyColumns[i][targetUnitPosition]);
					}
					remainingRange--;
				}
			}

			return receiversList;
		}

		public Tuple<List<IUnit>, List<IUnit>> GetUnitPairs(List<IUnit> armyOne, List<IUnit> armyTwo)
		{
            Tuple<List<IUnit>, List<IUnit>> tuple = new Tuple<List<IUnit>, List<IUnit>>(new List<IUnit>(), new List<IUnit>());

            int index = 0;
            while (index < 3 && index < armyOne.Count)
                tuple.Item1.Add(armyOne[index++]);
            
            index = 0;
            while (index < 3 && index < armyTwo.Count)
                tuple.Item2.Add(armyTwo[index++]);

            return tuple;
		}

		public bool CanUseThisStrategy(List<IUnit> armyOne, List<IUnit> armyTwo)
		{
			if (armyOne.Count >= 3 && armyTwo.Count >= 3)
				return true;
			return false;
		}
    }
}
