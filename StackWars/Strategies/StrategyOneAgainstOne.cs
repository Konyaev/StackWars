﻿using System;
using System.Collections.Generic;
namespace StackWars
{
    public class StrategyOneAgainstOne : IStrategy
    {
        public List<IUnit> GetSpecialActionReceiversByRange(ISpecialAbility unit, int position, List<IUnit> baseArmy, List<IUnit> enemyArmy)
        {
            List<IUnit> receiversList = new List<IUnit>();

            if (unit.Strength > 0) //enemy army action
            {
                int remainingRange = unit.Range - position;
                int unitToTakePosition = 0; //позиция юнита - ресивера
                while (remainingRange > 0 && enemyArmy.Count > unitToTakePosition)
                {
                    if (unit.CanDoSpecialActionWith(enemyArmy[unitToTakePosition]))
                        receiversList.Add(enemyArmy[unitToTakePosition]);
                    unitToTakePosition++;
                    remainingRange--;
                }
            }
            else //base army action
            {
                int baseArmyLength = baseArmy.Count;
                int positionShift = 1;

                while (positionShift <= unit.Range)
                {
                    if (position + positionShift < baseArmyLength)
                        if (unit.CanDoSpecialActionWith(baseArmy[position + positionShift]))
                            receiversList.Add(baseArmy[position + positionShift]);


                    if (position - positionShift >= 0)
                        if (unit.CanDoSpecialActionWith(baseArmy[position - positionShift]))
                            receiversList.Add(baseArmy[position - positionShift]);

                    positionShift++;
                }
            }

            return receiversList;
        }

        public Tuple<List<IUnit>, List<IUnit>> GetUnitPairs(List<IUnit> armyOne, List<IUnit> armyTwo)
        {
            return new Tuple<List<IUnit>, List<IUnit>>(new List<IUnit> { armyOne[0] }, new List<IUnit> { armyTwo[0] });
        }

        public bool CanUseThisStrategy(List<IUnit> armyOne, List<IUnit> armyTwo)
        {
            return true;
        }
    }
}
