﻿using System;
using System.Collections.Generic;
namespace StackWars
{
    public interface IStrategy
    {
        List<IUnit> GetSpecialActionReceiversByRange(ISpecialAbility unit, int position, List<IUnit> baseArmy, List<IUnit> enemyArmy);

        Tuple<List<IUnit>, List<IUnit>> GetUnitPairs(List<IUnit> armyOne, List<IUnit> armyTwo);

        bool CanUseThisStrategy(List<IUnit> armyOne, List<IUnit> armyTwo);
    }
}
