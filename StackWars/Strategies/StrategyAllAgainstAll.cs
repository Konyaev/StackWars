﻿using System;
using System.Collections.Generic;
namespace StackWars
{
    public class StrategyAllAgainstAll : IStrategy
    {
		public List<IUnit> GetSpecialActionReceiversByRange(ISpecialAbility unit, int position, List<IUnit> baseArmy, List<IUnit> enemyArmy)
        {
            List<IUnit> receiversList = new List<IUnit>();

			int startPosition = position - unit.Range;
			if (startPosition < 0)
				startPosition = 0;
            if (unit.Strength > 0) //special action должен действовать на вражескую армию
            {
                int endPosition = position + unit.Range;
                if (endPosition >= enemyArmy.Count)
                    endPosition = enemyArmy.Count - 1;
                for (int i = startPosition; i <= endPosition; i++)
                {
					if (unit.CanDoSpecialActionWith(enemyArmy[i]))
						receiversList.Add(enemyArmy[i]);
                }
            }
            else if (unit.Strength <= 0) //special action должен действовать на свою армию
			{
				int endPosition = position + unit.Range;
                if (endPosition >= baseArmy.Count)
                    endPosition = baseArmy.Count - 1;
				for (int i = startPosition; i <= endPosition; i++)
				{
                    if (unit.CanDoSpecialActionWith(baseArmy[i]))
                        receiversList.Add(baseArmy[i]);
				}
            }

            return new List<IUnit>();
        }

		public Tuple<List<IUnit>, List<IUnit>> GetUnitPairs(List<IUnit> armyOne, List<IUnit> armyTwo)
        {
			List<IUnit> armyOneUnits = new List<IUnit>();
			List<IUnit> armyTwoUnits = new List<IUnit>();
            int smallestArmyLength = Math.Min(armyOne.Count, armyTwo.Count);
            for (int i = 0; i < smallestArmyLength; i++)
            {
                armyOneUnits.Add(armyOne[i]);
                armyTwoUnits.Add(armyTwo[i]);
			}

            return new Tuple<List<IUnit>, List<IUnit>>(armyOneUnits, armyTwoUnits);
        }

		public bool CanUseThisStrategy(List<IUnit> armyOne, List<IUnit> armyTwo)
		{
            return true;
		}
    }
}
