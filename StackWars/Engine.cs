﻿using System;
using System.Collections.Generic;
using System.Linq;
namespace StackWars
{
    public class Engine
    {
        private List<IUnit> armyOne;

        private List<IUnit> armyTwo;

        public List<IUnit> ArmyOne
        {
            get => armyOne;
            set
            {
                armyOne = value;
                RefreshGameOver();
            }
        }

        public List<IUnit> ArmyTwo
        {
            get => armyTwo;
            set
            {
                armyTwo = value;
                RefreshGameOver();
            }
        }

        private IStrategy strategy = new StrategyOneAgainstOne();

        public IStrategy Strategy
        {
            get => strategy;
            set
            {
                if (value.CanUseThisStrategy(armyOne, armyTwo))
                    strategy = value;
                else
                    throw new Exception("Численность армий меньше необходимой для использования данной стратегии");
            }
        }

        public bool GameOver
        {
            get;
            private set;
        }

        private static Engine instance;

		private Engine()
		{
			armyOne = ArmyCreator.CreateArmy();
			armyTwo = ArmyCreator.CreateArmy();
		}

        public static Engine Instance
        {
            get
            {
				if (instance == null)
				{
                    instance = new Engine();
				}
                return instance;
            }
        }

        public void Hit(IUnit attacker, IUnit victim)
        {
            if (attacker.Health <= 0)
                return;
            int damage = attacker.Damage - victim.Defence;
            if (damage < 0)
                damage = 0;
            victim.ChangeHealth(victim.Health - damage);
        }

        private void RemoveDeadUnits()
        {
            armyOne.RemoveAll(unit => unit.Health <= 0);
            armyTwo.RemoveAll(unit => unit.Health <= 0);
        }

        //Вторая часть хода со specialAbility
        private void SecondStep(Tuple<List<IUnit>, List<IUnit>> meleeUnits)
        {
            int index = 0;
            foreach (IUnit unit in armyOne.ToList())
            {
                if (unit is ISpecialAbility)
                {
                    if (!meleeUnits.Item1.Contains(unit))
                        if (Services.ProbabilityBasedRandom(((ISpecialAbility)unit).ActionProbability))
                        {
                            List<IUnit> actionReceivers = strategy.GetSpecialActionReceiversByRange((ISpecialAbility)unit, index, armyOne, armyTwo);
                            if (actionReceivers.Count > 0)
                            {
                                int actionReceiverIndex = Services.random.Next(actionReceivers.Count);
                                ((ISpecialAbility)unit).DoSpecialAction(actionReceivers[actionReceiverIndex], armyOne);
                            }
                        }
                }
                index++;
            }

            index = 0;
            foreach (IUnit unit in armyTwo.ToList())
            {
                if (unit is ISpecialAbility)
                {
                    if (!meleeUnits.Item2.Contains(unit))
                        if (Services.ProbabilityBasedRandom(((ISpecialAbility)unit).ActionProbability))
                        {
                            List<IUnit> actionReceivers = strategy.GetSpecialActionReceiversByRange((ISpecialAbility)unit, index, armyTwo, armyOne);
							if (actionReceivers.Count > 0)
							{
                            int actionReceiverIndex = Services.random.Next(actionReceivers.Count);
								((ISpecialAbility)unit).DoSpecialAction(actionReceivers[actionReceiverIndex], armyTwo);
							}
                        }
                }
                index++;
            }
        }

        private void FirstStep(Tuple<List<IUnit>, List<IUnit>> meleeUnits)
        {
            int length = meleeUnits.Item1.Count;
            for (int i = 0; i < length; i++)
            {
                Hit(meleeUnits.Item1[i], meleeUnits.Item2[i]);
                Hit(meleeUnits.Item2[i], meleeUnits.Item1[i]);
            }

        }

        private void RefreshGameOver()
        {
            GameOver = (armyOne.Count <= 0 || armyTwo.Count <= 0);
        }

        public void Round()
        {
            if (GameOver == false)
            {
				Tuple<List<IUnit>, List<IUnit>> meleeUnits = strategy.GetUnitPairs(armyOne, armyTwo);
				FirstStep(meleeUnits);
				SecondStep(meleeUnits);
				RemoveDeadUnits(); //cобираeм трупы
				RefreshGameOver();
            }
            else
            {
                throw new Exception("Игра уже окончена");
            }
        }
    }
}
